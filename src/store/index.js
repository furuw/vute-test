import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const Form = {
  namespaced: true,
  state: {
    nextButton: ['確認', '確認', '送信'],
    component: ['Shuha', 'Ninzu', 'kakunin']
  },
  mutations: {},
  actions: {
    nextButtonAction ({ commit, state, rootState }) {
      console.log('nextButtonAction')
      commit('setStepCountUp', null, {root: true}) // rootへのアクセス
    },
    prevButtonAction ({ commit, state, rootState }) {
      console.log('prevButtonAction')
      commit('setStepCountDown', null, {root: true}) // rootへのアクセス
    }
  },
  getters: {
    getNextButton (state, getters, rootState) {
      return state.nextButton[rootState.stepCount]
    },
    getComponent (state, getters, rootState) {
      return state.component[rootState.stepCount]
    }
  }
}

export default new Vuex.Store({
  state: {
    stepCount: 0,
    property: [
      {
        religion: ''
      },
      {
        num: 0
      }
    ]
  },
  mutations: {
    setStepCountUp (state) {
      console.log('rootsetStepCount')
      state.stepCount++
    },
    setStepCountDown (state) {
      console.log('rootsetStepCount')
      state.stepCount--
    },
    setProperty (state, poroperty) {
      state.property[state.stepCount] = Object.assign(state.property[state.stepCount], poroperty)
    }
  },
  modules: {
    Form
  }
})
